#!/bin/bash
# no arguments, display a menu with option for info
# about processes

clear
while [ "$svar" != "9" ]
do
 echo ""
 echo "  1 - Hvem er jeg og hva er navnet på dette scriptet?"
 echo "  2 - Hvor lenge er det siden siste boot?"
 echo "  3 - Hva var gjennomsnittlig load siste minutt?"
 echo "  4 - Hvor mange prosesser og tråder finnes?"
 echo "  5 - Hvor mange context switch'er fant sted siste sekund?"
 echo "  6 - Hvor mange interrupts fant sted siste sekund?"
 echo "  9 - Avslutt dette scriptet"
 echo ""
 echo -n "Velg en funksjon: "
 read -r svar
 echo ""
 case $svar in
  1)clear
    echo "Jeg er $(whoami)"
    echo "Navnet på dette scriptet er "$0""
    read -r
    clear
    ;;
  2)clear
    echo "Oppetid: " $(uptime --pretty |awk '!($1="")')
    read -r
    clear
    ;;
  3)clear
    l=$(awk '{print $1}' /proc/loadavg)
    echo "Gjennomsnittlig load siste minutt: $l"
    read -r
    clear
    ;;
  4)clear
    echo "Antall prosesser og tråder: " \
    $(grep "^processes" /proc/stat | awk '!($1="")')
    read -r
    clear
    ;;
  5)clear
    before=$(grep "^ctxt" /proc/stat | awk '!($1="")')
    sleep 1
    after=$(grep "^ctxt" /proc/stat | awk '!($1="")')
    diff=$((after-before))
    echo "Antall context switcher siste sekund: $diff"
    read -r
    clear
    ;;
  6)clear
    before=$(grep "^intr" /proc/stat | awk '{print $2}')
    sleep 1
    after=$(grep "^intr" /proc/stat | awk '{print $2}')
    diff=$((after-before))
    echo "Antall interrupts siste sekund: $diff"
    read -r
    clear
    ;;
  9)echo Scriptet avsluttet
    exit
    ;;
  *)echo Ugyldig valg
    read -r
    clear
    ;;
 esac
done
