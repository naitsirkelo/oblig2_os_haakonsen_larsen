#!/bin/bash
for pid in $(pgrep chrome); do
  mpf=$(ps --no-headers -o maj_flt $pid)
  msg="Chrome $pid har forårsaket $mpf major page faults"
  if [ $mpf -gt 1000 ]
  then
    msg="$msg (mer enn 1000!)"
  fi
  echo $msg
done
