#!/bin/bash

dir=$1


if [ -d "$dir" ]; then #Checks if given directory exists

used=$(df "$dir" | awk 'FNR == 2 {print $5}')  #Get 2nd row 5th column from df, whcich gives used storage
echo "Partisjonen $dir befinner seg på er $used full."

biggestFile=""      #Help variables for echoing
biggestFileSize=0
mostHardlinksFile=""
mostHardlinks=0
totalSizeAllFiles=0
NoOfFiles=0


#for file in $(find $dir -type f)   #Iterate through all files in directory and in all subdirectories
while read -r -d '' file;
do
  
  NoOfFiles=$((NoOfFiles+1))    #Add 1 to total number of files found
  fileSize=$(stat --printf="%s" "$file")  #Get file size in bytes
  totalSizeAllFiles=$((totalSizeAllFiles+fileSize))  #Add to total
  if [ $fileSize -gt $biggestFileSize ]
  then                                #If current file is bigger than
    biggestFile="$file"               #earlier biggest, set this file to biggest
    biggestFileSize=$fileSize
  fi
  hardlinks=$(stat -c "%h" "$file")
  if [ $hardlinks -gt $mostHardlinks ] #If current files has more hardlinks
  then                                 #sets mostHardlinks to current file
    mostHardlinks=$hardlinks
    mostHardlinksFile="$file"
  fi

done < <(find "$dir" -type f -print0)

averageSize=$((totalSizeAllFiles/NoOfFiles)) #Calculates average size of files
bytesPretty=$(ls -lh "$biggestFile" | awk '{print $5}')  #Makes size into K/M/G/T etc
echo "Det finnes $NoOfFiles filer." #Print things
echo "Den største filen er "$biggestFile" som er $biggestFileSize ($bytesPretty) bytes stor"
echo "Gjennomsnittlig filstørrelse er ca $averageSize bytes"
echo "Filen "$mostHardlinksFile" har flest hardlinks, den har $mostHardlinks"

else
  echo "Directory med navn "$dir" finnes ikke"
fi
