# Obligatorisk oppgave 2 - Bash pipelines og scripting

Denne oppgaven består av de følgende laboppgavene fra kompendiet:

* 7.5.a (Prosesser og tråder)   
* 8.6.c (Page faults)   
* 8.6.d (En prosess sin bruk av virtuelt og fysisk minne)  
* 9.4.a (Informasjon om deler av filsystemet)  

## Gruppemedlemmer

* Marius Håkonsen
* Ole Larsen

## Oppgaveløsing

### 7.5.a

Bruk: **bash myprocinfo.bash**  
Scriptet benytter seg av følgende kommandoer i utdraget, basert på brukerens valg, operasjon 1-6.  
Utdrag:  
```
echo "Jeg er $(whoami)"
...
uptime --pretty |awk '!($1="")'
...
awk '{print $1}' /proc/loadavg
...
grep "^processes" /proc/stat | awk '!($1="")'
...
before=$(grep "^ctxt" /proc/stat | awk '!($1="")')
sleep 1
after=$(grep "^ctxt" /proc/stat | awk '!($1="")')
...
before=$(grep "^intr" /proc/stat | awk '{print $2}')
sleep 1
after=$(grep "^intr" /proc/stat | awk '{print $2}')
```

### 8.6.c

Bruk: **bash chromemajPF.bash**  
Looper gjennom alle prosessene av chrome som blir funnet. Finner så antall Major Page Faults, skriver ut disse, og redigerer meldingen om de er flere enn 1000.  
Utdrag:  
```
for pid in $(pgrep chrome); do
  ...
  mpf=$(ps --no-headers -o maj_flt $pid)
  msg="Chrome $pid har forårsaket $mpf major page faults"
  if [ $mpf -gt 1000 ]
      ...
  echo $msg
```

### 8.6.d

Bruk: **bash procmi.bash 'process id'**  
Henter informasjon om prosessen basert på id skrevet inn i kommandolinje. Om prosessen ikke finnes skrives det en feilmelding som i utdraget under. Fra utdraget: Bytter *VmSize* med relevant navn på minnet for å finne riktig informasjon.  
Utdrag:  
```
for pid in "$@"
do
  if [ -d "/proc/$pid" ];
  then
      ...
      grep "^VmSize" /proc/$pid/status | awk '{print $2}'
      ...
  else
    echo "Prosess med pid $pid finnes ikke"
  fi
```

### 9.4.a

Bruk: **bash fsinfo.bash 'directory'**  
Tar et directory som argument og benytter følgende kommandoer for å gi riktig informasjon. På innsiden av *while* loopen sjekkes alle filene i mappen og scriptet registrerer relevant data og overwriter lagret fil når/om den finner en større eller med flere hardlinks.  
Utdrag:  
```
if [ -d "$dir" ]; then
  ...
  used=$(df "$dir" | awk 'FNR == 2 {print $5}')
  echo "Partisjonen $dir befinner seg på er $used full."
  ...
  while read -r -d '' file;
  do
      stat --printf="%s" "$file"    # File size.
      stat -c "%h" "$file"  # Nr. of hardlinks.
  done < <(find "$dir" -type f -print0)
  ...
ls -lh "$biggestFile" | awk '{print $5}'    # Size type of largest file.
```

## Kvalitetssikring

For å kvalitetssikre C-koden i dette prosjektet har vi brukt clang-tidy. Denne er implementert som en pipeline på bitbucket, slik at når kode pushes ut til repoet, så blir den automatisk sjekket for typiske feil. På denne måten er man litt tryggere på at koden holder en viss kvalitet.
