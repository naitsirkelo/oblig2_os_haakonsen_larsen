#!/bin/bash

for pid in "$@"
do
  if [ -d "/proc/$pid" ];
  then
    file="meminfo/$pid-$(date +%Y%m%d-%H:%M:%S).meminfo"
    echo "******** Minne info om prosess med PID $pid ********" >> $file
    VmSize=$(grep "^VmSize" /proc/$pid/status | awk '{print $2}')
    echo "Total bruk av virtuelt minne (VmSize): $VmSize kB" >> $file
    VmStk=$(grep "^VmStk" /proc/$pid/status | awk '{print $2}')
    VmExe=$(grep "^VmExe" /proc/$pid/status | awk '{print $2}')
    PVm=$((VmSize+VmStk+VmExe))
    echo "Mengde privat virtuelt minne (VmData+VmStk+VmExe): $PVm kB" >> $file
    VmLib=$(grep "^VmLib" /proc/$pid/status | awk '{print $2}')
    echo "Mengde shared virtuelt minne (VmLib): $VmLib kB" >> $file
    VmRSS=$(grep "^VmRSS" /proc/$pid/status | awk '{print $2}')
    echo "Total bruk av fysisk minne (VmRSS): $VmRSS kB" >> $file
    VmPTE=$(grep "^VmPTE" /proc/$pid/status | awk '{print $2}')
    echo "Mengde fysisk minne som benyttes til page table (VmPTE): $VmPTE kB" >> $file
  else
    echo "Prosess med pid $pid finnes ikke"
  fi
done
